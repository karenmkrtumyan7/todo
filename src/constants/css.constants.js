export const classNames = {
    LIST: 'list',
    ITEM: 'item',
    ADD_ITEM: 'additem',
    SECOND: 'second',
    NAV: 'nav',
    SEARCH: 'search',
    BUTTON: 'button',
    CENTER: 'center'
}

export const alts = {
    DELETE_ICON: 'Delete Icon'
}

export const placeholders = {
    SEARCH: 'Search...',
    FUTURE_PLANS: 'Type here your future plans...'
}

