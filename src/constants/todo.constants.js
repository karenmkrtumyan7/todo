const ALL = 'all';
const CLICKED = 'clicked';
const DONE = 'done';
const ACTIVE = 'active';

export { ALL, CLICKED, DONE, ACTIVE };