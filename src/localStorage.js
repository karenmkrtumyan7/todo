function setToStorage(name, item) {
    localStorage.setItem(name, JSON.stringify(item));
}

function removeFromStorage(name) {
    localStorage.removeItem(name)
}

function getAllItems() {
    const keys = Object.keys(localStorage);
    const items = keys.map((key) => {
        const strItem = localStorage.getItem(key);
        return JSON.parse(strItem)
    });
    return items;
}

export { setToStorage, removeFromStorage, getAllItems }