import React, { useState } from 'react';
import './App.css';
import { Header } from './components/Header';
import { Items } from './components/Items';
import { ACTIVE, ALL, DONE } from './constants/todo.constants';
import { nanoid } from 'nanoid';
import { getAllItems, removeFromStorage, setToStorage } from './localStorage';
import { Item } from './helpers'

function App() {
  const defaultItems = getAllItems();
  let [value, setValue] = useState('');
  let [term, setTerm] = useState('');
  let [filter, setFilter] = useState(ALL);
  let [items, setItems] = useState(defaultItems);

  const giveId = () => {
    return nanoid();
  }

  const valueHandleChange = (e) => {
    setValue(e.target.value);
  }

  const termHandleChange = (e) => {
    setTerm(e.target.value);
  }

  const changeStatus = (id) => {
    for (let i = 0; i < items.length; i++) {
      if (id === items[i].id) {
        const startArr = items.slice(0 , i);
        const finishArr = items.slice(i + 1);
        const { message, id, status } = items[i];
        const newStatus = status === DONE ? ACTIVE : DONE;
        let change = new Item(message, id, newStatus);

        setToStorage(id, change);
        setItems([...startArr, change, ...finishArr]);
        break;
      }
    }
  }

  const changeFilter = (filter) => setFilter(filter) ;

  const addItem = (event) => {
    event.preventDefault();
    
    if (value.trim() !== '') {
      const newItem = new Item(value, giveId(), ACTIVE);
      setToStorage(newItem.id, newItem);
      setValue('');
      setItems([...items, newItem]);
    }
  }

  const deleteItem = (id) => {
    let index;
    let itemId;

    for (let i = 0; i < items.length; i++) {
      itemId = items[i].id;
      if (itemId === id) {
        index = i;
        break;
      }
    }

    const startArr = items.slice(0 , index);
    const finishArr = items.slice(index + 1);

    removeFromStorage(id);
    setItems([...startArr, ...finishArr ]);
  }

  const searching = (term, items) => {
    if (!term.length) {
      return items;
    }
   return items.filter(function(obj) {
      const messageToLowerCase = obj.message.toLowerCase();
      return messageToLowerCase.includes(term.toLowerCase());
    })
  }
  
  const doFilter = (items, filter) => {
    switch(filter) {
      case ALL: return items;
      case ACTIVE: return items.filter((el) => el.status === ACTIVE);
      case DONE: return items.filter((el) => el.status === DONE);
      default: return items;
    }
  } 

  const search = doFilter(searching(term, items), filter);
  
  return (
    <div className="App">
      <div>
        <Header valueHandleChange = {valueHandleChange} 
          value = {value}
          addItem = {addItem}
          term = {term} 
          termHandleChange = {termHandleChange}
          changeFilter = {changeFilter}
          filter = {filter} />
        <Items items = {search}
          changeStatus ={changeStatus}
          deleteItem = {deleteItem}/>
      </div>
    </div>
  )
 };

export default App;
