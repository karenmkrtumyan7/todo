import React from 'react';
import './Header.css'
import { Nav } from '../Nav';
import { classNames, placeholders } from '../../constants/css.constants';

export const Header = (props) => {
    const { valueHandleChange, value, addItem, 
      changeFilter, filter, term, termHandleChange } = props;
    const { ADD_ITEM, SECOND, NAV, SEARCH, BUTTON } = classNames;
    const { SEARCH: SEARCH_PL, FUTURE_PLANS } = placeholders;
    
    return (
      <header>
        <h1>TodoApp</h1>
        <form onSubmit={addItem}>
          <input type='text' 
            onChange={valueHandleChange}
            value={value}
            className={ADD_ITEM}
            placeholder={FUTURE_PLANS} />
        </form>
        <button onClick={addItem}
          className={BUTTON}>Send</button>
        <div className={SECOND}>
          <Nav changeFilter={changeFilter}
            filter={filter}
            className={NAV}/>
          <input type='text'
            onChange={termHandleChange}
            value={term}
            className={SEARCH}
            placeholder={SEARCH_PL}/>
        </div>
      </header>
    )
  }
