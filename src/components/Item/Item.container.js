import React, { useEffect, useState } from 'react';
import { Item } from './Item';
import { setToStorage } from '../../localStorage';

function ItemContainer(props) {
    const { changeStatus, id, message, status } = props;
    let [value, setValue] = useState(message);
    const input = React.createRef();
 
    useEffect(() => {
        input.current.addEventListener('dblclick', changeStatus(id));
        const inputCopy = input.current;

        return () => {
            inputCopy.removeEventListener('dblclick', changeStatus(id));
        }          
    }, [changeStatus, id, input]);

    const onChangeHandler = (event) => {
        const item = {
            message: event.target.value,
            status, id
        };
        setToStorage(id, item);
        setValue(event.target.value);
    }

    return <Item {...props}
                value={value}
                setValue={setValue}
                input={input}
                onChangeHandler={onChangeHandler}/>
}

export { ItemContainer as Item }
