import React from 'react';
import DeleteIcon from '../../assets/delete.png';
import { alts, classNames } from '../../constants/css.constants';


export function Item(props) {
    const { deleteItem, status, id,
        input, value, onChangeHandler } = props;
	const { ITEM } = classNames;
	const { DELETE_ICON } = alts;

    return (
        <div className={ITEM}>
            <input type='text' 
              ref={input}
              className={status} 
              value={value}
              onChange={onChangeHandler}/>
            <div onClick={deleteItem(id)}>
              <img src={DeleteIcon} alt={DELETE_ICON}/>
            </div>
        </div>
    )
}