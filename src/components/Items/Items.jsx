import React from 'react';
import './Items.css';
import { classNames } from '../../constants/css.constants';
import { Item } from '../Item';

const Items = (props) => {
	const { items } = props;
	const { LIST } = classNames;

  return (
    <div className={LIST}>
      {items.map((el) => (
        <Item key={el.id}
              {...el}
              {...props}/>
      ))}
    </div>
  )
  }

export default Items;