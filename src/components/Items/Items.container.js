import React from 'react';
import Items from './Items';

function ItemsContainer(props) {
    const changeStatus = (id) => () => props.changeStatus(id);
    const deleteItem = (id) => () => props.deleteItem(id);

    return <Items {...props}
                changeStatus={changeStatus}
                deleteItem={deleteItem}/>
}

export { ItemsContainer as Items }