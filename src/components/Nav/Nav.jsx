import React from 'react';
import { classNames } from '../../constants/css.constants';
import { ACTIVE, ALL, DONE } from '../../constants/todo.constants';
import './Nav.css';

const Nav = (props) => {
    const { allClass, activeClass, doneClass, changeFilter } = props;
    const { CENTER } = classNames;

    return (
      <ul className={ALL}>
        <li onClick={changeFilter(ALL)} 
          className={allClass}>All</li>
        <li onClick={changeFilter(ACTIVE)} 
          className={`${CENTER} ${activeClass}`}>Active</li>
        <li onClick={changeFilter(DONE)} className = {doneClass}>Done</li>
      </ul>
    )
  }

export { Nav };