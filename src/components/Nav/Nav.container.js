import React from 'react';
import { ACTIVE, ALL, CLICKED, DONE } from '../../constants/todo.constants';
import { Nav } from './Nav';

function NavContainer(props) {
    const { changeFilter: _changeFilter, filter } = props;
    const allClass = filter === ALL ? CLICKED : ''; 
    const activeClass = filter === ACTIVE ? CLICKED : ''; 
    const doneClass = filter === DONE ? CLICKED : '';
    const changeFilter = (newFilter) => () => _changeFilter(newFilter);

    return (
        <Nav allClass={allClass}
            activeClass={activeClass}
            doneClass={doneClass}
            changeFilter={changeFilter}/>
    )
} 

export { NavContainer as Nav }